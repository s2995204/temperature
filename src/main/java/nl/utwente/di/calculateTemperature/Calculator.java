package nl.utwente.di.calculateTemperature;

public class Calculator {
    double getCalculatedTemperature(String temperature){
        double tempCelsius = Double.parseDouble(temperature);
        return (tempCelsius * 9/5) + 32;
    }
}
