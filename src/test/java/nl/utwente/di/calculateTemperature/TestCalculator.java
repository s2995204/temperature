package nl.utwente.di.calculateTemperature;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/** Tests the Quoter*/
public class TestCalculator {
    @Test
    public void testBook1 () throws Exception {
        Calculator calculator = new Calculator();
        double price = calculator.getCalculatedTemperature("20");
        assertEquals(68.0, price, 0.0, "20 Celsius value in Fahrenheit: ") ;
    }
}